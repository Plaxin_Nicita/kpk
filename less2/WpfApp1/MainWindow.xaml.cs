﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

     

        private void new_gro_txt_MouseMove(object sender, MouseEventArgs e)
        {   if (new_gro_txt.Text == "" || new_gro_txt.Text == "новое")
            {
                new_gro_txt.Text = "назвать группу";
            }
            
        }

        private void exit_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (MessageBox.Show("выключть пк?", "выключить?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                Process.Start("shutdown", "/s /t 0");
            }
        }

        private void scrol_out_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.Width >= 1200)
            { 
            stack_out.Orientation = Orientation.Horizontal;
            }
            if (this.Width <= 1200)
            {
                stack_out.Orientation = Orientation.Vertical;
            }

        }
    }
}
